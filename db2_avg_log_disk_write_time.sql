with t1 as 
  ( select t.snapshot_time , rownumber() over ( order by t.snapshot_time ) as rn , 
           t.log_writes , 
           t.log_write_time , 
           t.num_log_write_io 
           from transaction_log_mon_history t where t.snapshot_time > ( current timestamp - 10 minutes ) ) , 
    t2 as ( select   snapshot_time ,  rn +1  as rn2 , log_writes, num_log_write_io, log_write_time from t1 ) 
    SELECT date(t1.snapshot_time) || '-' || time(t1.snapshot_time) || ' ' 
    || ' AVG_LOG_DISK_WRITE_TIME: ' || 
    case when t1.NUM_LOG_WRITE_IO - t2.num_log_write_io <= 0 then 0 
          else decimal( (decfloat(t1.LOG_WRITE_TIME - t2.log_write_time) / decfloat(t1.NUM_LOG_WRITE_IO - t2.num_log_write_io)), 9, 5 ) 
     end  as avg_log_disk_write_time 
        from t1 , t2 
        where t1.rn = t2.rn2 and (t1.NUM_LOG_WRITE_IO - t2.num_log_write_io > 0) 
        and ( decimal( (decfloat(t1.LOG_WRITE_TIME - t2.log_write_time) / decfloat(t1.NUM_LOG_WRITE_IO - t2.num_log_write_io)), 9, 5 )> 0) 
        order by  t1.snapshot_time  desc 
fetch first 1 rows only with ur ;

 
